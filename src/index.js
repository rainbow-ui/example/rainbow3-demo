import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import App from './App';
require('./styles/App.css');
window.ServerDate = function(){
    return new Date();
};
ReactDOM.render(
    <Router history={hashHistory}>
        <Route path="/" component={App}>
            <IndexRoute getComponent={
                (nextState, callback) => {
                    require.ensure([], (require) => {
                        callback(null, require('./pages/index'));
                    }, 'home');
                }} />
        </Route>
        <Route path="login" component={App}>
            <IndexRoute getComponent={
                (nextState, callback) => {
                    require.ensure([], (require) => {
                        callback(null, require('./pages/index'));
                    }, 'login');
                }} />
        </Route>
        <Route path='home' getComponent={
            (nextState, callback) => {
                require.ensure([], (require) => {
                    callback(null, require('./pages/task/index'));
                }, 'home');
            }} />
        <Route path='task' getComponent={
            (nextState, callback) => {
                require.ensure([], (require) => {
                    callback(null, require('./pages/task/index'));
                }, 'task');
            }} />
        <Route path='adjustment' getComponent={
            (nextState, callback) => {
                require.ensure([], (require) => {
                    callback(null, require('./pages/adjustment/index'));
                }, 'adjustment');
            }} />
    </Router>
    , document.getElementById('app'));
