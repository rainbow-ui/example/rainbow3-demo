import {UICardGroup, UICard, UIText, UISelect,UISmartPanelGrid,CodeTable,UIDataTable,UIColumn,UINumber,UIDateTimePicker,UICheckbox,UILink,UIPercent,UIDialog,UITextarea} from 'rainbowui-desktop-core';
import Component from '../components/component.js';

export default class App extends Component{
    constructor(props) {
        super(props);
        this.state = {
            CurrencyType: new CodeTable([
                {
                    id: 1,
                    text: 'RMB'
                },
                {
                    id: 2,
                    text: 'USD'
                }
            ]),
            InsuranceStatus: new CodeTable([
                {
                    id: 1,
                    text: 'Effective'
                },
                {
                    id: 2,
                    text: 'Not Effective'
                }
            ]),
            PaymentPlanTableList: [
                {
                    AdjustmentSequence: 1,
                    CoverageName: 'Illness hospitalization medical insurance',
                    AdjustedAmount: 0,
                    CompensationAmount: 0,
                    dataValue: this.props.claim.bill && this.props.claim.bill.hospitalCollect ? this.props.claim.bill.hospitalCollect : [],
                    tableHeaderOne: ['Hospital Name','Beijing Ditan Hospital','Document No.','1345689'],
                    tableHeaderTwo: ['Expense Item','Bill Amount','Category B','Category C Amount','Third Party Reimbursed','Unreasonable Amount','Actual Reportable Amount','Deductible','Payout Ratio','Excess','Compensation Amount','Chargeback Amount','Remarks']
                },
                {
                    AdjustmentSequence: 2,
                    CoverageName: 'Hospitalization Allowance',
                    AdjustedAmount: 0,
                    CompensationAmount: 0,
                    dataValue: [
                        {DaysInHospital: 10,
                            ActualReportableAmount: 600,
                            DeductibleDays: 0,
                            ExcessDays: 0,
                            CompensationAmount: 600
                        }
                    ],
                    tableHeaderOne: ['Hospital Name','Beijing Ditan Hospital','Document No.','1345689'],
                    tableHeaderTwo: ['Hospital Days','Actual Reportable Amount','Deductible Days','Excess Days','Compensation Amount']
                },
                {
                    AdjustmentSequence: null,
                    CoverageName: 'Accidental Injury Death Insurance',
                    AdjustedAmount: 0,
                    CompensationAmount: 0
                },
                {
                    AdjustmentSequence: null,
                    CoverageName: 'Accidental Injury And Disability Insurance',
                    AdjustedAmount: 0,
                    CompensationAmount: 0
                },
                {
                    AdjustmentSequence: null,
                    CoverageName: 'Accidental Injury Medical Insurance',
                    AdjustedAmount: 0,
                    CompensationAmount: 0
                },
                {
                    AdjustmentSequence: null,
                    CoverageName: 'Ambulance Cost Insurance',
                    AdjustedAmount: 0,
                    CompensationAmount: 0
                }
            ]
        };
    }

    render () {
        return (
            <div>
                <UICardGroup>
                    <UICard title="Policy Information">
                        <UISmartPanelGrid column="3">
                            <UIText label="PolicyNo." colspan='2' model={this.props.claim.policyList[this.props.policyIndex]} property="policyNo" enabled="false"/>
                        </UISmartPanelGrid>
                    </UICard>
                    <UICard title="Insurance Information">
                        <UISmartPanelGrid column="3">
                            <UIText label="Product Name" model={this.props.claim.policyList[this.props.policyIndex]} property="productName" enabled="false"/>
                            <UIText label="Insurance Status" model= {this.props.claim.policyList[this.props.policyIndex]} property="coverageStatusNow" enabled="false"/>
                            {/* <UISelect label='当前险种状态' codeTable={this.state.InsuranceStatus} model={this.props.claim.paymentPlan} property='CurrentInsuranceStatus'/> */}
                            <UIDateTimePicker label="Next Payment Date" model={this.props.claim.policyList[this.props.policyIndex]} property="NextPaymentDate" enabled="false"/>
                            <UIText label="Accident Time" model= {this.props.claim.policyList[this.props.policyIndex]} property="AccidentTime" enabled="false"/>
                            {/* <UISelect label='事故日险种状态' codeTable={this.state.InsuranceStatus} model={this.props.claim.paymentPlan} property='AccidentDayInsuranceStatus'/> */}
                            <UIDateTimePicker label="Last Expiration Date" model={this.props.claim.policyList[this.props.policyIndex]} property="LastExpirationDate" enabled="false"/>
                            <UIDateTimePicker label="Last Reinstatement Date" model={this.props.claim.policyList[this.props.policyIndex]} property="LastReinstatementDate" enabled="false"/>
                            <UINumber label="Increase Amount" model={this.props.claim.policyList[this.props.policyIndex]} property="IncreaseAmount" enabled="false"/>
                            <UIText label="Effective Date To Accident" model={this.props.claim.policyList[this.props.policyIndex]} property="EffectiveDateToAccident" enabled="false"/>
                            <UIText label="Reinstatement Date To Accident" model={this.props.claim.policyList[this.props.policyIndex]} property="ReinstatementDateToAccident" enabled="false"/>
                        </UISmartPanelGrid>
                    </UICard>
                    <UICard title="Accountability Adjustment">
                        <UIDataTable id="PaymentPlanTable" value={this.state.PaymentPlanTableList} pageable="false" rowDetailRender={(function (data) {
                            if(data.dataValue && data.tableHeaderOne && data.tableHeaderTwo){
                                return (
                                    <div>
                                        {this.renderChildTable(data)}
                                    </div>
                                );
                            }
                            return(<noscript/>);
                                
                        }).bind(this)
                        }>
                            <UIColumn width="5%" headerTitle='Select' render={(data) => {
                                return <UICheckbox model={data} id={'checkFlags' + data.dataIndex} single="true" property="checkFlag" onChange={this.changeCheckedCoverage.bind(this)} />;
                            }} />
                            <UIColumn headerTitle="Adjustment Sequence" value="AdjustmentSequence" width="auto" render={
                                (data) => {
                                    return <UINumber model={data} property="AdjustmentSequence" allowDecimal="false"/>;
                                }
                            } />
                            <UIColumn headerTitle="Coverage Name" value="CoverageName" width="auto" render={
                                (data) => {
                                    return <UIText model={data} property="CoverageName" io="out" />;
                                }
                            } />
                            <UIColumn headerTitle="Parameter" value="Parameter" width="auto" render={
                                (data) => {
                                    return <UIText model={data} property="Parameter" io="out" />;
                                }
                            } />
                            <UIColumn headerTitle="Adjusted Amount" value="AdjustedAmount" width="auto" render={
                                (data) => {
                                    return <UINumber model={data} property="AdjustedAmount" />;
                                }
                            } />
                            <UIColumn headerTitle="Compensation Amount" value="CompensationAmount" width="auto" render={
                                (data) => {
                                    return <UINumber model={data} property="CompensationAmount"/>;
                                }
                            } />
                            <UIColumn headerTitle="Accumulator" width="auto" render={
                                (data) => {
                                    return <UILink value="Detail" onClick={this.showDetailDialog.bind(this)}/>;
                                }
                            } />
                         
                        </UIDataTable>
                    </UICard>
                    <UICard>
                        <UISmartPanelGrid column="3">
                            <UINumber label="Total Insurance Coverage" model={this.props.claim.policyList[this.props.policyIndex]} property="TotalInsuranceCoverage"/>
                            <UISelect label='Claim Conclusion' codeTable={this.state.InsuranceStatus} model={this.props.claim.policyList[this.props.policyIndex]} property='ClaimConclusion'/>
                            <UISelect label='Products Continue To Work' codeTable={this.state.InsuranceStatus} model={this.props.claim.policyList[this.props.policyIndex]} property='ProductsContinueToWork'/>
                            <UIDateTimePicker label="Effective Date Of Termination" model={this.props.claim.policyList[this.props.policyIndex]} property="EffectiveDateOfTermination"/>
                            <UITextarea label="History Remarks" model={this.props.claim.policyList[this.props.policyIndex]} property="HistoryRemarks"/>
                        </UISmartPanelGrid>
                    </UICard>
                </UICardGroup>
                <UIDialog id="coverageAdjustmentDetail">
                    {this.renderDetail()}
                </UIDialog>
            </div>
        );
    }

    renderChildTable(data) {
        if(data.CoverageName == 'Illness hospitalization medical insurance'){
            return (
                <UICard title="Medical Documents">
                    <UIDataTable id="MedicalDocTable" styleClass="primary" detailable="true" indexable="false" detailVisible="0" value={data.dataValue} pageable="false" displayLength="5" renderTableHeader={this.getTableHeader.bind(this,data)}>
                        {/* <If condition={data.CoverageName == 'Illness hospitalization medical insurance'}> */}
                        <UIColumn headerTitle="Expense Item" value="billName" width="auto" render={
                            (data) => {
                                return <UIText model={data} property="billName" io="out" />;
                            }
                        } />
                        <UIColumn headerTitle="Bill Amount" value="billAmount" width="auto" render={
                            (data) => {
                                return <UINumber model={data} property="billAmount" io="out"/>;
                            }
                        } />
                        <UIColumn headerTitle="Category B" value="billSecondAmount" width="auto" render={
                            (data) => {
                                return <UINumber model={data} property="billSecondAmount" io="out"/>;
                            }
                        } />
                        <UIColumn headerTitle="Category C Amount" value="billThirdAmount" width="auto" render={
                            (data) => {
                                return <UINumber model={data} property="billThirdAmount" io="out"/>;
                            }
                        } />
                        <UIColumn headerTitle="Third Party Reimbursed" value="billThirdCost" width="auto" render={
                            (data) => {
                                return <UINumber model={data} property="billThirdCost" io="out"/>;
                            }
                        } />
                        <UIColumn headerTitle="Unreasonable Amount" value="billUnreasonAmount" width="auto" render={
                            (data) => {
                                return <UINumber model={data} property="billUnreasonAmount" io="out"/>;
                            }
                        } />
                        <UIColumn headerTitle="Actual Reportable Amount" value="actualAmount" width="auto" render={
                            (data) => {
                                return <UINumber model={data} property="actualAmount" io="out"/>;
                            }
                        } />
                        <UIColumn headerTitle="Deductible" value="Deductible" width="auto" render={
                            (data) => {
                                return <UINumber model={data} property="Deductible" />;
                            }
                        } />
                        <UIColumn headerTitle="Payout Ratio" value="PayoutRatio" width="auto" render={
                            (data) => {
                                return <UIPercent model={data} property="PayoutRatio" />;
                            }
                        } />
                        <UIColumn headerTitle="Excess" value="Excess" width="auto" render={
                            (data) => {
                                return <UINumber model={data} property="Excess" />;
                            }
                        } />
                        <UIColumn headerTitle="Compensation Amount" value="CompensationAmount" width="auto" render={
                            (data) => {
                                return <UINumber model={data} property="CompensationAmount" />;
                            }
                        } />
                        <UIColumn headerTitle="Chargeback Amount" value="ChargebackAmount" width="auto" render={
                            (data) => {
                                return <UINumber model={data} property="ChargebackAmount" io="out"/>;
                            }
                        } />
                        <UIColumn headerTitle="Remarks" value="Remarks" width="auto" render={
                            (data) => {
                                return <UIText model={data} property="Remarks" io="out" />;
                            }
                        } />
                        {/* </If> */}
                    </UIDataTable>
                </UICard>
            );
        }
        return(
            <UICard title="Medical Documents">
                <UIDataTable id="MedicalDocTable" styleClass="primary" detailable="true" indexable="false" detailVisible="0" value={data.dataValue} pageable="false" displayLength="5" renderTableHeader={this.getTableHeader.bind(this,data)}>
                    <UIColumn headerTitle="Hospital Days" value="DaysInHospital" width="auto" render={
                        (data) => {
                            return <UINumber model={data} property="DaysInHospital" io="out"/>;
                        }
                    } />
                    <UIColumn headerTitle="Actual Reportable Amount" value="ActualReportableAmount" width="auto" render={
                        (data) => {
                            return <UINumber model={data} property="ActualReportableAmount" io="out"/>;
                        }
                    } />
                    <UIColumn headerTitle="Deductible Days" value="DeductibleDays" width="auto" render={
                        (data) => {
                            return <UINumber model={data} property="DeductibleDays" io="out"/>;
                        }
                    } />
                    <UIColumn headerTitle="Excess Days" value="ExcessDays" width="auto" render={
                        (data) => {
                            return <UINumber model={data} property="ExcessDays" io="out"/>;
                        }
                    } />
                    <UIColumn headerTitle="Compensation Amount" value="CompensationAmount" width="auto" render={
                        (data) => {
                            return <UINumber model={data} property="CompensationAmount" io="out"/>;
                        }
                    } />
                </UIDataTable>
            </UICard>
        );
        
        
    }

    renderDetail(){
        return(
            <div style={{width: '850px',margin: 'auto'}}>
                <div class="adjustmentDetail">
                    <div class="adjustmentDetailCard">
                        <div class="adjustmentDetail-arrow">
                            <div>Input</div>
                            <div>--------></div>
                            <div>5080</div>
                        </div>
                        <div class="adjustmentDetail-list">
                            <div>AXDET0002</div>
                            <dl class="adjustmentDetailCard-dl">
                                <dt><UIText label="Product Name" io="out" layout="horizontal" value="Deduction"/></dt>
                                <dt><UIText label="Deduction Of Times Or Days" io="out" layout="horizontal" value="300.00"/></dt>
                                <dt><UIText label="Restriction Type" io="out" layout="horizontal" value="Product Guarantee Period"/></dt>
                                <dt><UIText label="total Deductible" io="out" layout="horizontal" value="300.00"/></dt>
                                <dt><UIText label="This Deduction Amount" io="out" layout="horizontal" value="0.00"/></dt>
                                <dt><UIText label="Accumulator Balance" io="out" layout="horizontal" value="0.00"/></dt>
                                <dt><UIText label="Excess Amount" io="out" layout="horizontal" value="0.00"/></dt>
                                <dt><UIText label="本次Compensation Amount" io="out" layout="horizontal" value="5,080.00"/></dt>
                                <dt style={{borderBottom: 'unset'}}><div style={{height: '22px'}}/></dt>
                                <dt><UIText label="本次Compensation Amount" value="5,080.00"/></dt>
                            </dl>
                        </div>
                    </div>
                    <div class="adjustmentDetailCard">
                        <div class="adjustmentDetail-arrow">
                            <div></div>
                            <div>--------></div>
                            <div>5080</div>
                        </div>
                        <div class="adjustmentDetail-list">
                            <div>AXCPY0002</div>
                            <dl class="adjustmentDetailCard-dl">
                                <dt><UIText label="Restriction Type" io="out" layout="horizontal" value="Share"/></dt>
                                <dt><UIText label="Payout Ratio" io="out" layout="horizontal" value="0.80"/></dt>
                                <dt><UIText label="Restriction Type" io="out" layout="horizontal" value="Product Guarantee Period"/></dt>
                                <dt><UIText label="Excess Amount" io="out" layout="horizontal" value="1,016.00"/></dt>
                                <dt><UIText label="This Compensation Amount" io="out" layout="horizontal" value="4,064.00"/></dt>
                                <dt style={{borderBottom: 'unset'}}><div style={{height: '121px'}}/></dt>
                                <dt><UIText label="This Compensation Amount" value="4,064.00"/> </dt>
                            </dl>
                        </div>
                    </div>
                    <div class="adjustmentDetailCard">
                        <div class="adjustmentDetail-arrow">
                            <div></div>
                            <div>--------></div>
                            <div>5080</div>
                        </div>
                        <div class="adjustmentDetail-list">
                            <div>AXDSL0002</div>
                            <dl class="adjustmentDetailCard-dl">
                                <dt><UIText label="Restriction Type" io="out" layout="horizontal" value="Limit Of Liability"/></dt>
                                <dt><UIText label="Service Limit" io="out" layout="horizontal" value="0.00"/></dt>
                                <dt><UIText label="Amount Limit" io="out" layout="horizontal" value="10,000.00"/></dt>
                                <dt><UIText label="Restriction Type" io="out" layout="horizontal" value="Product Guarantee Period"/></dt>
                                <dt><UIText label="Previous Total" io="out" layout="horizontal" value="10,000.00"/></dt>
                                <dt><UIText label="This Cumulative Amount" io="out" layout="horizontal" value="0.00"/></dt>
                                <dt><UIText label="Accumulator Balance" io="out" layout="horizontal" value="0.00"/></dt>
                                <dt><UIText label="Excess Amount" io="out" layout="horizontal" value="0.00"/></dt>
                                <dt><UIText label="This Compensation Amount" io="out" layout="horizontal" value="5,080.00"/></dt>
                                <dt><UIText label="This Compensation Amount" value="5,080.00"/></dt>
                            </dl>
                        </div>
                    </div>
                    <div class="adjustmentDetailCard-last">
                        <div class="adjustmentDetail-arrow">
                            <div>Output</div>
                            <div>--------></div>
                            <div>5080</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    showDetailDialog(){
        UIDialog.show('coverageAdjustmentDetail');
    }

    getTableHeader(data) {
        // if($('#MedicalDocTableThead').children().length > 0){
        //     $('#icle').empty();
        // }
        // let $tr = [];
        // let $rowone = $('<tr></tr>');
        // for(let i = 0; i < data.tableHeaderOne.length; i++) {
        //     $('<th colSpan=\'2\'>' + data.tableHeaderOne[i] + '</th>').appendTo($rowone);
        // }
        // $tr.push($rowone);
        // let $rowtwo = $('<tr></tr>');
        // for(let i = 0; i < data.tableHeaderTwo.length; i++) {
        //     $('<th colSpan=\'2\'>' + data.tableHeaderTwo[i] + '</th>').appendTo($rowtwo);
        // }
        // $tr.push($rowtwo);
        // for(let j = 0;j < $tr.length;j++){
        //     $($tr[j]).appendTo($('#MedicalDocTableThead'));
        // }
        let trDomOne = [];
        for(let i = 0; i < data.tableHeaderOne.length; i++) {
            trDomOne.push(
                <th colSpan='3'>{data.tableHeaderOne[i]}</th>
            );
        }
        let trDomTwo = [];
        for(let i = 0; i < data.tableHeaderTwo.length; i++) {
            trDomTwo.push(
                <th >{data.tableHeaderTwo[i]}</th>
            );
        }
        return (
            <thead id="MedicalDocTableThead">
                <tr>
                    {trDomOne}
                </tr>
                <tr>
                    {trDomTwo}
                </tr>
            </thead>

        );
    }

    paymentPlan(){
        this.props.changePage('2');
    }

    changeCheckedCoverage(){}

}