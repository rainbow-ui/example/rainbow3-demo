import {UICardGroup, UICard, UIText, UISelect,UISmartPanelGrid,CodeTable,UIDateTimePicker,UIDataTable,UIColumn,UINumber,UITextarea } from 'rainbowui-desktop-core';
import Component from '../components/component.js';

export default class App extends Component{
    constructor(props) {
        super(props);
        this.props.claim.claimAdjust = this.props.claim.claimAdjust ? this.props.claim.claimAdjust : {};
        this.state = {
            claim: this.props.claim,
            Gender: new CodeTable([
                {
                    id: 1,
                    text: '男'
                },
                {
                    id: 2,
                    text: '女'
                }
            ]),
            IdType: new CodeTable([
                { id: '01', text: 'ID Card'},
                { id: '02', text: 'Military ID'},
                { id: '03', text: 'Other'},
                { id: '04', text: 'Passport'}
            ]),
            claimType: new CodeTable([
                { id: '0', text: 'Please Select'},
                { id: '1', text: 'Accident'},
                { id: '2', text: 'Disease'},
                { id: '3', text: 'Female Fertility'},
                { id: '4', text: 'Major Diseases'},
                { id: '5', text: 'Physical Examination'},
                { id: '6', text: 'Pending'}
            ]),
            Caselevel: new CodeTable([
                { id: '01', text: 'Appeal Case'},
                { id: '02', text: 'Major Case'},
                { id: '03', text: 'Regular Cases'},
                { id: '04', text: 'Special Case'}
            ]),
            ApprovingPersonnel: new CodeTable([]),
            claimAdjustTableList: [
                {
                    policyNo: 'S1000834177522550',
                    policyClaimAmount: 600,
                    claimAmount: 600,
                    AssignedAmount: 0,
                    adjustAmount: 0
                }
            ],
            claimAdjustHistoryTableList: []
        };
    }

    render () {
        return (
            <div>
                <UICardGroup>
                    <UICard title="Case Adjustment">
                        <UISmartPanelGrid column="3">
                            <UIText label="Insured" model={this.props.claim.claimInfo} property="insuredName" enabled="false"/>
                            <UISelect label='Gender' codeTable={this.state.Gender} model={this.props.claim} property='GenderCode' enabled="false"/>
                            <UISelect label='ID Type' codeTable={this.state.IdType} model={this.props.claim.applicantInfo} property='certiType' enabled="false"/>
                            <UIText label="ID No." model={this.props.claim.applicantInfo} property='certiNo' enabled="false"/>
                            <UISelect label='Claim Type' codeTable={this.state.claimType} model={this.props.claim.claimInfo} property='claimType' enabled="false"/>
                            <UISelect label='Case Level' codeTable={this.state.Caselevel} model={this.props.claim} property='accidentLevel' enabled="false"/>
                            <UIDateTimePicker label="Report Date" model={this.props.claim.claimInfo} property="reportDate" enabled="false"/>
                            <UIDateTimePicker label="Date Of Outbreak" model={this.props.claim.claimInfo} property="DateOfOutbreak" enabled="false"/>
                            <UIDateTimePicker label="Date Of Hospitalization" model={this.props.claim.claimInfo} property="hospitalizationDate" enabled="false"/>
                            <UIDateTimePicker label="Discharge date" model={this.props.claim.claimInfo} property="leaveHospitalDate" enabled="false"/>
                        </UISmartPanelGrid>
                        <UIDataTable id="claimAdjustTable" value={this.props.claim.policyList} pageable="false">
                            <UIColumn headerTitle="PolicyNo" value="policyNo" width="auto" render={
                                (data) => {
                                    return <UIText model={data} property="policyNo" io="out" />;
                                }
                            } />
                            <UIColumn headerTitle="Total Policy Benefit Amount" value="policyClaimAmount" width="auto" render={
                                (data) => {
                                    return <UINumber model={data} property="policyClaimAmount" io="out" />;
                                }
                            } />
                            <UIColumn headerTitle="Claim Amount" value="claimAmount" width="auto" render={
                                (data) => {
                                    return <UINumber model={data} property="claimAmount" io="out" />;
                                }
                            } />
                            <UIColumn headerTitle="Assigned Amount" value="AssignedAmount" width="auto" render={
                                (data) => {
                                    return <UINumber model={data} property="AssignedAmount" io="out" />;
                                }
                            } />
                            <UIColumn headerTitle="Adjust Amount" value="adjustAmount" width="auto" render={
                                (data) => {
                                    return <UINumber model={data} property="adjustAmount" io="out" />;
                                }
                            } />
                        </UIDataTable>
                    </UICard>
                    <UICard title="Case Denial History">
                        <UIDataTable id="claimAdjustHistoryTable" value={this.state.claimAdjustHistoryTableList} pageable="false">
                            <UIColumn headerTitle="Date Of Denial" value="DateOfDenial" width="auto" render={
                                (data) => {
                                    return <UIDateTimePicker model={data} property="DateOfDenial" io="out"/>;
                                }
                            } />
                            <UIColumn headerTitle="Approving Personnel" value="ApprovingPersonnel" width="auto" render={
                                (data) => {
                                    return <UIText model={data} property="ApprovingPersonnel" io="out" />;
                                }
                            } />
                            <UIColumn headerTitle="Remarks" value="Remarks" width="auto" render={
                                (data) => {
                                    return <UITextarea model={data} property="Remarks" io="out" />;
                                }
                            } />
                        </UIDataTable>
                        <UISmartPanelGrid column="3">
                            <UINumber label="Self Amount/Day" model={this.props.claim.claimAdjust} property="selfAmount"/>
                            <UINumber label="Excess Amount/Day" model={this.props.claim.claimAdjust} property="ExcessAmount"/>
                            <UINumber label="Exemption Amount/Day" model={this.props.claim.claimAdjust} property="ExemptionAmount"/>
                            <UINumber label="Payment Amount" model={this.props.claim.claimAdjust} property="PaymentAmount"/>
                            <UIText label="ReceiptNo." model={this.props.claim.claimAdjust} property="ReceiptNo"/>
                            <UISelect label='Approving Personnel' codeTable={this.state.ApprovingPersonnel} model={this.props.claim.claimAdjust} property='ApprovingPersonnel'/>
                            <UINumber label="ActualAmount" model={this.props.claim.claimAdjust} property="ActualAmount"/>
                            <UITextarea label="History Remarks" model={this.props.claim.claimAdjust} property="HistoryRemarks"/>
                            <UITextarea label="Remarks" model={this.props.claim.claimAdjust} property="Remarks"/>
                            <UITextarea label="Basis Of Compensation" model={this.props.claim.claimAdjust} property="BasisOfCompensation"/>
                        </UISmartPanelGrid>
                    </UICard>
                </UICardGroup>
            </div>
        );
    }
}