
import { UIPage, UIDataTable, UIColumn, UISmartPanelGrid, UIBox, UIButton, UICardGroup, UICard, UIText, UISelect, CodeTable, UIDateTimePicker, UILink, UIDrawer, UILabel } from 'rainbowui-desktop-core';
import { TopCard } from 'vela-parent-ui';
import Menu from '../components/menu.js';
import Component from '../components/component.js';
import logo from '../../images/ebao_logo.svg';
import { SessionContext } from 'rainbow-desktop-cache';

export default class Task extends Component {
    constructor(props) {
        super(props);
        SessionContext.put('curMenuId',1);
        this.state = {
            showDrawer: false,
            task: {},
            taskNamesList: null,
            taskStatesList: null,
            firmList: null,
            taskTypeList: null,
            tableList: null,
            filterTable: null,
            interval: 2000,
            greetingTime: ''
        };
    }

    componentWillMount() {
        const taskNames = new CodeTable([
            { id: '0', text: 'Please Select'},
            { id: '1', text: 'Report'},
            { id: '2', text: 'Notice'},
            { id: '3', text: 'Adjustment'},
            { id: '4', text: 'Sign Off'},
            { id: '5', text: 'Report reply'},
            { id: '6', text: 'Committee response'}
        ]);
        const taskStates = new CodeTable([
            { id: '0', text: 'Please Select'},
            { id: '1', text: 'Unclaimed'},
            { id: '2', text: 'Claimed'}
        ]);
        const firms = new CodeTable([
            { id: '0', text: 'Please Select'},
            { id: '1', text: 'Headquarters'}
        ]);
        const taskType = new CodeTable([
            { id: '0', text: 'Please Select'},
            { id: '1', text: 'Accident'},
            { id: '2', text: 'Disease'},
            { id: '3', text: 'Female Fertility'},
            { id: '4', text: 'Major Diseases'},
            { id: '5', text: 'Physical Examination'},
            { id: '6', text: 'Pending'}
        ]);
        const table = [
            { taskName: 'Adjustment', gender: '2', identify: '4', idNo: '320102195902214321', taskState: 'Claimed', claimNo: '4000000888888171000009602', auditDate: '', registerDate: '03/12/2017', policyNo: 'S100000007P002171003269711', firm: 'Headquarters', person: 'Amy' },
            { taskName: 'Adjustment', gender: '1', identify: '4', idNo: '320102199802214321', taskState: 'Claimed', claimNo: '4000000888888171000009501', auditDate: '', registerDate: '01/12/2019', policyNo: 'S100000007P002171003269710', firm: 'Headquarters', person: 'Jake' },
            { taskName: 'Adjustment', gender: '1', identify: '4', idNo: '320102199102214321', taskState: 'Claimed', claimNo: '4000000888888171000009312', auditDate: '', registerDate: '05/12/2017', policyNo: 'S100000007P002171003269715', firm: 'Headquarters', person: 'Coco' },
            { taskName: 'Adjustment', gender: '1', identify: '4', idNo: '320102199102214321', taskState: 'Claimed', claimNo: '4000000888888171000009313', auditDate: '', registerDate: '05/12/2017', policyNo: 'S100000007P002171003269716', firm: 'Headquarters', person: 'Amy' }
        ];
        const currentDate = new Date();
        const hours = currentDate.getHours();
        let text;
        if (hours >= 0 && hours <= 10) {
            text = 'Good Morning';
        } else if (hours > 10 && hours <= 14) {
            text = 'Good Afternoon';
        } else if (hours > 14 && hours <= 18) {
            text = 'Good Afternoon';
        } else if (hours > 18 && hours <= 24) {
            text = 'Good Evening';
        }
        this.setState({
            taskNamesList: taskNames ,
            tableList: table,
            taskStatesList: taskStates ,
            firmList: firms,
            taskTypeList: taskType,
            filterTable: table,
            greetingTime: text
        });
    }

    onClaim(data) {
        switch(data.taskName) {
        case 'Adjustment':
            window.location.hash = '/adjustment';
            break;
        }
    }

    onPolicy(data) {
        SessionContext.put('currentTask',data);
        switch(data.taskName) {
        case 'Adjustment':
            window.location.hash = '/adjustment';
            break;
        }
    }

    renderTable () {
        return (
            <UIDataTable id="TaskTable" className="head-inner-checkbox" pageable="true" value={this.state.filterTable}>
                <UIColumn headerTitle="PolicyNo" render={
                    (data) => {
                        return (<UILink value={data.policyNo} noI18n="true" onClick={this.onPolicy.bind(this,data)}/>);
                    }
                } />
                <UIColumn headerTitle="ClaimNo" render={
                    (data) => {
                        return (<UILink value={data.claimNo} noI18n="true" onClick={this.onClaim.bind(this,data)}/>);
                    }
                } />
                <UIColumn headerTitle="Task Name" render={
                    (data) => {
                        return (<UIText io="out" model={data} property="taskName" />);
                    }
                } />
                <UIColumn headerTitle="Task State" render={
                    (data) => {
                        return (<UIText io="out" model={data} property="taskState" />);
                    }
                } />
                <UIColumn headerTitle="Report Date" render={
                    (data) => {
                        return (<UIText io="out" model={data} property="registerDate" />);
                    }
                } />
                <UIColumn headerTitle="Audit Date" render={
                    (data) => {
                        return (<UIText io="out" model={data} property="auditDate" />);
                    }
                } />
                <UIColumn headerTitle="Branch Company" render={
                    (data) => {
                        return (<UIText io="out" model={data} property="firm" />);
                    }
                } />
                <UIColumn headerTitle="Insured" render={
                    (data) => {
                        return (<UIText io="out" model={data} property="person" />);
                    }
                } />
            </UIDataTable>
        );
    }

    onLink() {
        this.setState({
            showDrawer: true
        });
    }

    closeDrawer() {
        this.setState({
            showDrawer: false
        });
    }

    renderDrawer() {
        return (
            <UIDrawer open={this.state.showDrawer} styleClass="fix-bottom-drawer" title="更多" onClose={this.closeDrawer.bind(this)} width="285px">
                <div class="drawer-body">
                    <UISmartPanelGrid column='1'>
                        <UISelect label="Branch Company" clean="false" model={this.state.task} conditionMap={{0: '请选择'}} property="firm" codeTable={this.state.firmList}/>
                        <UIDateTimePicker label="Report Date" defaultValue={String(new Date())} showDeleteIcon="false" model={this.state.policy} property="startTime" />
                        <UIDateTimePicker label="To" defaultValue={String(new Date())} model={this.state.policy} property="startTime" />
                        <UISelect label="Claim Type" clean="false" model={this.state.task} conditionMap={{0: '请选择'}} property="taskType" codeTable={this.state.taskTypeList}/>
                        <UIText label="Insured" model={this.state.task} property="person"/>
                    </UISmartPanelGrid>
                </div>
                <div class="drawer-float-button">
                    <UIBox>
                        <UIButton value="Search" styleClass="primary" onClick={this.onDrawerSearch.bind(this)}/>
                    </UIBox>
                </div>
            </UIDrawer>
        );
    }

    onDrawerSearch() {
        this.setState({
            showDrawer: false
        });
    }

    onSearch() {
        const taskName = this.state.task.taskName;
        const taskState = this.state.task.taskState;
        const policyNo = this.state.task.policyNo;
        const res = this.state.tableList.filter( e => {
            let isTaskNameOk, isTaskStateOk;
            if(taskName) {
                if(Number(taskName) < 1) {
                    isTaskNameOk = true;
                } else {
                    isTaskNameOk = (this.state.taskNamesList.map[Number(taskName)].text === e.taskName);
                }
            } else {
                isTaskNameOk = true;
            }
            if(taskState) {
                if(Number(taskState) < 1) {
                    isTaskStateOk = true;
                } else {
                    isTaskStateOk = (this.state.taskStatesList.map[Number(taskState)].text === e.taskState);
                }
            } else {
                isTaskStateOk = true;
            }
            return isTaskStateOk && isTaskNameOk && (policyNo ? policyNo === e.policyNo : true);
        });
        this.setState({
            filterTable: res
        });
    }

    onReset() {
        this.state.task.taskName = '-1';
        this.state.task.taskState = '-1';
        this.state.task.policyNo = '';
        this.setState({
            task: this.state.task,
            filterTable: []
        });
    }
    

    render () {
        return (
            <Menu logo={logo} onClick={this.goToPage.bind(this)}>
                <UIPage>
                    <div class="fix-inner-height">
                        <TopCard>
                            <div id='task-top-card'>
                                <div id='task-top-card-left'>
                                    <div id="task-avatar">
                                        <img src='https://rainbow.ebaotech.com/static/rainbow/image/avatar.png' width='8%'/>
                                    </div>
                                    <div id='task-top-card-text-area'>
                                        <UILabel label={`${this.state.greetingTime}！`} size="3x" />
                                        <marquee scrolldelay='170'>No announcement today.</marquee>
                                    </div>
                                </div>
                                <div class='statistics-block'>
                                    <div>
                                        <div class='statistics-title'>My Tasks</div>
                                        <div class='danger'>2</div>
                                    </div>
                                    <div class='seperator'>
                                        <div class='statistics-title'>Total Tasks</div>
                                        <div class='success'>{this.state.tableList ? this.state.tableList.length : this.state.filterTable.length}</div>
                                    </div>
                                </div>
                            </div>
                        </TopCard>
                        {this.renderDrawer()}
                        <UICardGroup>
                            <UICard style={{ 'margin-top': '15px' }}>
                                <UISmartPanelGrid column="4">
                                    <UISelect label="Task NAme" clean="false" model={this.state.task} conditionMap={{0: 'Please Select'}} property="taskName" codeTable={this.state.taskNamesList}/>
                                    <UISelect label="Task Status" clean="false" model={this.state.task} conditionMap={{0: 'Please Select'}} property="taskState" codeTable={this.state.taskStatesList}/>
                                    <UIText label="PolicyNo" model={this.state.task} property="policyNo"/>
                                    <div id='tasks-middle-btn-box'>
                                        <UIBox>
                                            <UIButton value="Search" styleClass="primary" onClick={this.onSearch.bind(this)}/>
                                            <UIButton value="Reset" styleClass="default" onClick={this.onReset.bind(this)}/>
                                            {/* <UILink value="更多" onClick={this.onLink.bind(this)}/> */}
                                        </UIBox>
                                    </div>
                                </UISmartPanelGrid>
                            </UICard>
                            <UICard title='Search Result'>
                                {this.renderTable()}
                            </UICard>
                        </UICardGroup>
                    </div>
                </UIPage>
            </Menu>
        );
    }
}